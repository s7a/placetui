module.exports = {
 
  entry: {
  javascript: "./dist/bundle.js",
  html: "./src/html/index.html",
  },
  output: {
    path: './dist',
    filename: 'bundle.js'
  },
  module: {
  loaders: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      loaders: ["babel-loader"],
    },
    {
  	test: /\.html$/,
  	loader: "file?name=[name].[ext]",
	}
  ],

},
};