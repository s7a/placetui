var React = require('react');
var ReactDOM = require('react-dom');
var Tweet = require('./Tweet.jsx');


var TweetList = React.createClass({



  render: function() {
    var TweetNodes = this.props.tweets.map((Tweett) => {
          return (
                  <Tweet content={Tweett.content} picURL={Tweett.picURL} likes={Tweett.likes} retweets={Tweett.retweets}
                    username={Tweett.username} nick={Tweett.nick} key={Tweett.key}>

                  </Tweet>



          );
        }
    );



    return (
      <div className="TweetList">
        {TweetNodes}
      </div>
    );
  }
});


module.exports = TweetList;
