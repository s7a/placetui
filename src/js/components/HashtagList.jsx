var React = require('react');
var ReactDOM = require('react-dom');
var Hashtag = require('./Hashtag.jsx');


var HashtagList = React.createClass({


  render: function() {
    var HashtagNodes = this.props.data.map((hashtag) => {
          return (
                  <Hashtag hashtagName={hashtag.name} key={hashtag.id} occu={hashtag.occurrence}
                  collapseHashtags={this.props.collapseHashtags} tweets={hashtag.tweets}>
                  </Hashtag>



          );
        }
    );



    return (
      <div className="HashtagList">
        {HashtagNodes}
      </div>
    );
  }
});


module.exports = HashtagList;
