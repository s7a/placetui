var React = require('react');
var ReactDOM = require('react-dom');
var CitySelect = require('./CitySelect.jsx');
var Button = require('react-bootstrap/lib/Button');


var HashtagForm = React.createClass({
  getInitialState: function(){

    return {city: '', isSubBtnDisabled: true, clearInput: false};
  },
  handleCityChange: function(e){
    this.setState({city: e.city});
    this.setState({isSubBtnDisabled: false});
  },
  handleInvalidCityInput:function(){
    this.setState({isSubBtnDisabled: true});
  },

  handleInputCleared:function(){
    this.setState({clearInput: false});
  },

  handleSubmit: function(e){
    e.preventDefault();
    this.props.onLocationSubmit({city: this.state.city});
    this.setState({city: ''});
    this.setState({clearInput: true});
    this.setState({isSubBtnDisabled: true});

  },
  render: function() {
    return (
        <form className="commentForm" onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-8">
              <CitySelect  onCityChange={this.handleCityChange} onInvalidInput={this.handleInvalidCityInput}
                           clearInput={this.state.clearInput} onInputCleared={this.handleInputCleared}/>
            </div>
            <div className="col-md-4">
              <Button bsStyle="primary" bsSize="small" type="submit" disabled={this.state.isSubBtnDisabled}>get Hashtags</Button>
            </div>
          </div>
        </form>
    );
  }
});

module.exports = HashtagForm;
