var React = require('react');
var ReactDOM = require('react-dom');
var marked = require('marked');
var Image = require('react-bootstrap/lib/Image');
var Panel = require('react-bootstrap/lib/Panel');
var Badge = require('react-bootstrap/lib/Badge');

var Tweet = React.createClass({


    render: function(){
        return (

            <div className='tweet'>
              <Panel bsClass="tweetPanel panel">
                <div>
                  <a>
                    <Image src={this.props.picURL} circle width="44" height="44"/>
                    <strong>{this.props.username}</strong>
                    <span><s>@</s><b>{this.props.nick}</b></span>
                  </a>
                </div>
                </Panel>
                <Panel bsClass="tweetTextPanel panel">
                <div>
                  <span className='tweetText'>{this.props.content}</span>
                  <div>
                    <Image src="../src/assets/Retweet.png" width="15" height="15"/>
                    <span className="retweetCount">{this.props.retweets}</span>
                    <Image src="../src/assets/Like.png" width="8" height="8"/>
                    <span className="likesCount">{this.props.likes}</span>
                  </div>
                </div>
              </Panel>
            </div>

        );
    }
});

module.exports = Tweet;
