var React = require('react');
var ReactDOM = require('react-dom');
var TweetList = require('./TweetList.jsx');
var marked = require('marked');
var Fade = require('react-bootstrap/lib/Fade');
var Button = require('react-bootstrap/lib/Button');

var Hashtag = React.createClass({
    rawMarkup: function() {
        var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
        return { __html: rawMarkup };
    },


    render: function(){
        return (
            <Fade in={this.props.collapseHashtags}>
            <div className='comment'>
                <Button bsStyle="link" bsSize="large" bsClass="tweetLink btn">
                    #{this.props.hashtagName}
                </Button>
                <TweetList tweets={this.props.tweets}/>

            </div>
            </Fade>
        );
    }
});

module.exports = Hashtag;
