var React = require('react');
var ReactDOM = require('react-dom');
var Autosuggest = require('react-autosuggest');

const cities = [
  {
    name: 'Dublin',
    lat: 53.350140 ,
    lon: -6.266155
  },
  {
    name: 'Berlin',
    lat: 52.520008 ,
    lon: 13.404954
  },
  {
    name: 'Trier',
    lat: 49.75 ,
    lon: 6.6333333
  },
  {
    name: 'Katowice',
    lat: 50.270908 ,
    lon: 19.039993
  },
];

function getSuggestions(value) {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;


  return inputLength === 0 ? [] : cities
.filter(cit =>
    cit.name.toLowerCase().slice(0, inputLength) === inputValue
  );
}

function getSuggestionValue(suggestion) {

  return suggestion.name;                                             
}

function renderSuggestion(suggestion) {
  return (
    <span>{suggestion.name}</span>
  );
}

class CitySelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      suggestions: getSuggestions('')
    };

    this.onChange = this.onChange.bind(this);
    this.onSuggestionsUpdateRequested = this.onSuggestionsUpdateRequested.bind(this);
  }

  onChange(event, { newValue }) {
    this.setState({
      value: newValue
    });
    var location = cities.filter(cit => cit.name.toLowerCase() == newValue.toLowerCase());
    if(location.length == 1){
        this.props.onCityChange({city : location});

    }else{
      this.props.onInvalidInput();
    }
    this.props.onInputCleared();
    
  }

  onSuggestionsUpdateRequested({ value }) {
    this.setState({
      suggestions: getSuggestions(value)
    });
  }

  render() {

    var value = this.state.value;
    var suggestions = this.state.suggestions;
    if(value.lenght > 0 || this.props.clearInput){
      value = '';
    }
    
    const inputProps = {
      placeholder: 'City',
      value,
      onChange: this.onChange,
    };

    return (
      <Autosuggest suggestions={suggestions}
                   onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
                   getSuggestionValue={getSuggestionValue}
                   renderSuggestion={renderSuggestion}
                   inputProps={inputProps}
                    />
    );
  }
}

module.exports = CitySelect;