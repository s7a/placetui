/**
 * Created by Arturooo on 25/02/2016.
 */

 var placetRestUrlTemplate='http://localhost:8081/rest/hashtags?lat=[lat]&lon=[lon]&rd=50&lang=en&city=[city]&count=10';
function setLocationInRestUrl(city){
	var url = placetRestUrlTemplate.replace('[lat]', city[0].lat);
	url = url.replace('[lon]', city[0].lon);
	url = url.replace('[city]', city[0].name);

	return url;

};
