var React = require('react');
var ReactDOM = require('react-dom');
var HashtagList = require('./components/HashtagList.jsx');
var HashtagForm = require('./components/HashtagForm.jsx');
var Data = require('./components/Data.jsx');


var HashtagBox = React.createClass({
  getHashTagsFromServer: function(url){
    $.ajax({
      url: url,
      dataType: 'json',
      cache: false,
      success: function(data){
        this.setState({data: data, collapseHashtags: true});
      }.bind(this),
      error: function(xhr, status, err){
        console.error(url, status, err.toString());
        this.setState({data: [    {"name":"John", "id":1,occurrence: 7, "tweets":[{"content":"Blabal", "picURL":"../src/assets/arturo.jpg", "likes":99, "retweets":10,"username":"Nick Floaty","nick":"URooo","key":1}]},
                                  {"name":"Anna", "id":2,occurrence: 6, "tweets":[{"content":"HeyHeyHey this is a very long tweet Text", "picURL":"../src/assets/arturo.jpg", "likes":99, "retweets":10,"username":"Arto Donald","nick":"Lapiiii","key":2}]},
                                  {"name":"Peter","id":3,occurrence: 5, "tweets":[{"content":"Super Duper Tweet", "picURL":"../src/assets/arturo.jpg", "likes":99, "retweets":10, "username":"Ato Donald","nick":"Dr.Doom","key":3}]}
                              ], collapseHashtags: true});
      }.bind(this)

    });
  },
  handleLocationSubmit: function(location){
    this.setState({collapseHashtags: false});
    var url=setLocationInRestUrl(location.city);
    this.getHashTagsFromServer(url);

  },
  getInitialState: function(){
    return {data: [], collapseHashtags: false};
  },
  componentDidMount: function(){
    var url=setLocationInRestUrl([{name: 'Dublin',lat: 53.350140 ,lon: -6.266155}]);
    this.getHashTagsFromServer(url);

  },
  render: function() {
    return (
      <div>
        <h1 className="text-center">placeT</h1>
        <HashtagForm onLocationSubmit={this.handleLocationSubmit}/>
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
            <HashtagList data={this.state.data} collapseHashtags={this.state.collapseHashtags} />
          </div>
        </div>
      </div>
    );
  }
});

ReactDOM.render(
    <HashtagBox />,
  document.getElementById('example')
);
